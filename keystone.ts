import { config } from '@keystone-next/keystone';

import { lists } from './schema';
import { withAuth, db, session, server, graphql, ui } from './config';

export default withAuth(
  config({
    server,
    db,
    ui,
    graphql,
    lists,
    session,
  })
);
