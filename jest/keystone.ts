import { config } from '@keystone-next/keystone';
import { withAuth, ui, graphql, session } from '../config';
import { lists } from '../schema';

export default withAuth(
    config({
        db: {
            provider: 'sqlite',
            url: process.env.DATABASE_URL || 'file:./jest.db',
        },
        ui,
        graphql,
        lists,
        session,
    })
);
