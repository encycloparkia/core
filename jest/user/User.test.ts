import { KeystoneContext } from "@keystone-next/keystone/types";
import { setupTestEnv, TestEnv } from "@keystone-next/keystone/testing";
import config from "../keystone";

interface User {
  id: string;
  name: string;
  email: string;
  role: {
    id: string;
    name: string;
    canManageContent: boolean;
    canManageUsers: boolean;
  };
}

const mapToUser = (data: any): User => {
  return {
    id: data.id,
    name: data.name,
    email: data.email,
    role: {
      id: data.role.id,
      name: data.role.name,
      canManageContent: data.role.canManageContent,
      canManageUsers: data.role.canManageUsers,
    },
  };
};

describe("User", () => {
  let testEnv: TestEnv, context: KeystoneContext;
  let administrator: User;
  let member: User;

  beforeAll(async () => {
    testEnv = await setupTestEnv({ config });
    context = testEnv.testArgs.context;

    await testEnv.connect();

    const users = await context.sudo().query.User.createMany({
      data: [
        {
          name: "Steve",
          email: "steve@example.com",
          password: "super-secret",
          role: {
            create: {
              name: "Administrator",
              canManageContent: true,
              canManageUsers: true,
            },
          },
        },
        {
          name: "Alice",
          email: "alice@example.com",
          password: "super-secret",
          role: {
            create: {
              name: "Member",
              canManageContent: false,
              canManageUsers: false,
            },
          },
        },
      ],
      query: "id name email role { id name canManageContent canManageUsers }",
    });

    administrator = mapToUser(users[0]);
    member = mapToUser(users[1]);
  });

  afterAll(async () => {
    await testEnv.disconnect();
  });

  test("Check that administrator password is set", async () => {
    const { password } = await context.query.User.findOne({
      where: { id: administrator.id },
      query: "password { isSet }",
    });
    expect(password.isSet).toEqual(true);
  });

  test("Check that member password is set", async () => {
    const { password } = await context.query.User.findOne({
      where: { id: member.id },
      query: "password { isSet }",
    });
    expect(password.isSet).toEqual(true);
  });

  // test("Update steve's own email address", async () => {
  //   const { data } = await context
  //     .withSession({
  //       itemId: steve.id,
  //       data: {
  //         id: steve.id,
  //         name: "Steve",
  //         role: { canManageUsers: false, canManageContent: false },
  //       },
  //     })
  //     .graphql.raw({
  //       query: `mutation update($id: ID!) {
  //         updateUser(where: { id: $id }, data: { email: "new-email@example.com" }) {
  //           email
  //         }
  //       }`,
  //       variables: { id: steve.id },
  //     });

  //   expect(data!.updateUser.email).toEqual("new-email@example.com");
  // });

  // test("Update alice's email address with steve's session", async () => {
  //   const { data, errors } = await context
  //     .withSession({
  //       itemId: steve.id,
  //       data: {
  //         id: steve.id,
  //         name: "Steve",
  //         role: { canManageUsers: false, canManageContent: false },
  //       },
  //     })
  //     .graphql.raw({
  //       query: `mutation update($id: ID!) {
  //         updateUser(where: { id: $id }, data: { email: "new-email@example.com" }) {
  //           email
  //         }
  //       }`,
  //       variables: { id: alice.id },
  //     });

  //   expect(data!.updateUser).toBe(null);
  //   expect(errors).toHaveLength(1);
  //   expect(errors![0].path).toEqual(["updateUser"]);
  //   expect(errors![0].message).toEqual(
  //     `Access denied: You cannot perform the 'update' operation on the item '{"id":"${alice.id}"}'. It may not exist.`
  //   );
  // });
});
