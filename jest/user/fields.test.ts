import { KeystoneContext } from "@keystone-next/keystone/types";
import { setupTestEnv, TestEnv } from "@keystone-next/keystone/testing";
import config from "../keystone";

interface User {
  id: string;
  name: string;
  email: string;
  role: {
    id: string;
    name: string;
    canManageContent: boolean;
    canManageUsers: boolean;
  };
}

describe("User - Fields", () => {
  let testEnv: TestEnv, context: KeystoneContext;

  const createFunction = async (string: string) => {
    return context.sudo().graphql.raw({
      query: string,
    });
  };

  describe("name", () => {
    beforeEach(async () => {
      testEnv = await setupTestEnv({ config });
      context = testEnv.testArgs.context;

      await testEnv.connect();
    });

    afterEach(async () => {
      await testEnv.disconnect();
    });

    test("String - OK", async () => {
      const createMutationString = `mutation { createUser(data: { name: "Alex", email: "alex@example.com", password: "super-secret" }) { id name } }`;
      const { data, errors } = await createFunction(createMutationString);
      expect(data!.createUser.name).toEqual("Alex");
      expect(errors).toBe(undefined);
    });

  });
});
