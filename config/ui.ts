import { AdminUIConfig } from "@keystone-next/keystone/types";
import { rules } from "../schema/access";

export const ui: AdminUIConfig = {
  isAccessAllowed: rules.canUseAdminUI,
};
