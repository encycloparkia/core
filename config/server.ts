import { ServerConfig } from "@keystone-next/keystone/types";
import '../env';

let port = process.env.KEYSTONE_PORT ? parseInt(process.env.KEYSTONE_PORT) : 3000;

export const server: ServerConfig = {
  cors: {
    credentials: true,
    origin: ["http://localhost:3000/", "https://studio.apollographql.com"],
  },
  port,
  maxFileSize: 200 * 1024 * 1024,
};
