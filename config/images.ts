export const images = {
  upload: "local",
  local: {
    storagePath: "public/images",
    baseUrl: "/images",
  },
};
