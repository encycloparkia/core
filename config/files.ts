export const files = {
  upload: "local",
  local: {
    storagePath: "public/files",
    baseUrl: "/files",
  },
};
