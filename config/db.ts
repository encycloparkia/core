import { DatabaseConfig } from "@keystone-next/keystone/types";
import '../env';

const url = process.env.DB_URL;

if (url === undefined) throw new Error("Error");

export const db: DatabaseConfig = {
  provider: "postgresql",
  url,
  onConnect: async () => {
    console.log("✨ Database connected");
  },
  enableLogging: false,
  useMigrations: true,
  idField: { kind: "cuid" },
};
