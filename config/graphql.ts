import {
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginLandingPageDisabled,
} from "apollo-server-core";

export const graphql = {
  path: "/api/graphql",
  queryLimits: { maxTotalResults: 100 },
  cors: {
    origin: ["http://localhost:3000/", "https://studio.apollographql.com"],
    credentials: true,
  },
  apolloConfig: {
    plugins: [
      process.env.NODE_ENV === "production"
        ? ApolloServerPluginLandingPageDisabled()
        : ApolloServerPluginLandingPageGraphQLPlayground(),
    ],
  },
};
