import { password, relationship, text } from "@keystone-next/keystone/fields";
import { list } from "@keystone-next/keystone";

import { permissions, rules } from "../access";

const fieldModes = {
  editSelfOrRead: ({ session, item }: any) =>
    permissions.canManageUsers({ session }) || session.itemId === item.id
      ? "edit"
      : "read",
  editSelfOrHidden: ({ session, item }: any) =>
    permissions.canManageUsers({ session }) || session.itemId === item.id
      ? "edit"
      : "hidden",
};

export const User = list({
  access: {
    operation: {
      create: () => true,
    },
    filter: {
      query: () => true,
      update: ({session}) => rules.canManageUserList({session}),
      delete: rules.canManageUserList,
    },
  },
  ui: {
    hideCreate: (context) => !permissions.canManageUsers(context),
    hideDelete: (context) => !permissions.canManageUsers(context),
    itemView: {
        defaultFieldMode: (context) => {
          const permi = permissions.canManageUsers(context) ? "edit" : "hidden";
          console.log(permi);
          return permi
        },
    },
    listView: {
      defaultFieldMode: (context) => permissions.canManageUsers(context) ? "read" : "hidden",
      initialColumns: ["name", "email", "role"],
      initialSort: { field: 'name', direction: 'ASC' },
    },
  },
  fields: {
    name: text({
      ui: {
        itemView: { fieldMode: fieldModes.editSelfOrRead },
      },
    }),
    email: text({
      isIndexed: "unique",
      validation: {
        isRequired: true,
      },
      access: {
        read: rules.canManageUser,
      },
      ui: {
        itemView: { fieldMode: fieldModes.editSelfOrHidden },
      },
    }),
    password: password({
      validation: {
        isRequired: true,
      },
      ui: {
        itemView: { fieldMode: fieldModes.editSelfOrHidden },
      },
    }),
    role: relationship({
      ref: "Role.users",
      access: permissions.canManageUsers,
      isFilterable: true,
      isOrderable: true,
    }),
  },
});
